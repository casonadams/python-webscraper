import os

import pytest

ENV_VARS = os.environ
OPTIONS = "OPTIONS"


@pytest.fixture
def chrome_options(chrome_options):
    if OPTIONS in ENV_VARS:
        for option in os.environ.get(OPTIONS).split(","):
            chrome_options.add_argument(f"--{option}")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--page_load_strategy=none")
    chrome_options.add_argument("--window-size=800,600")
    return chrome_options


@pytest.fixture
def firefox_options(firefox_options):
    if OPTIONS in ENV_VARS:
        for option in os.environ.get(OPTIONS).split(","):
            firefox_options.add_argument(f"--{option}")
    firefox_options.add_argument("--disable-gpu")
    firefox_options.add_argument("--window-size=800,600")

    # speed up firefox seems to shave off about 10 seconds
    # https://erayerdin.hashnode.dev/how-to-make-selenium-load-faster-with-firefox-in-python-ck7ncjyvw00sd8ss1v4i5xob1
    firefox_options.set_preference("browser.cache.memory.capacity", 65536)
    firefox_options.set_preference("browser.chrome.toolbar_style", 1)
    firefox_options.set_preference("browser.display.show_image_placeholders", False)
    firefox_options.set_preference("browser.display.use_document_colors", False)
    firefox_options.set_preference("browser.display.use_document_fonts", 0)
    firefox_options.set_preference("browser.display.use_system_colors", True)
    firefox_options.set_preference("browser.formfill.enable", False)
    firefox_options.set_preference("browser.helperApps.deleteTempFileOnExit", True)
    firefox_options.set_preference("browser.pocket.enabled", False)
    firefox_options.set_preference("browser.shell.checkDefaultBrowser", False)
    firefox_options.set_preference("browser.startup.homepage", "about:blank")
    firefox_options.set_preference("browser.startup.homepage", "about:blank")
    firefox_options.set_preference("browser.startup.page", 0)
    firefox_options.set_preference("browser.tabs.forceHide", True)
    firefox_options.set_preference("browser.urlbar.autoFill", False)
    firefox_options.set_preference("browser.urlbar.autocomplete.enabled", False)
    firefox_options.set_preference("browser.urlbar.showPopup", False)
    firefox_options.set_preference("browser.urlbar.showSearch", False)
    firefox_options.set_preference("content.notify.interval", 500000)
    firefox_options.set_preference("content.notify.ontimer", True)
    firefox_options.set_preference("content.switch.threshold", 250000)
    firefox_options.set_preference("extensions.checkCompatibility", False)
    firefox_options.set_preference("extensions.checkUpdateSecurity", False)
    firefox_options.set_preference("extensions.update.autoUpdateEnabled", False)
    firefox_options.set_preference("extensions.update.enabled", False)
    firefox_options.set_preference("general.startup.browser", False)
    firefox_options.set_preference("loop.enabled", False)
    firefox_options.set_preference("network.http.pipelining", True)
    firefox_options.set_preference("network.http.pipelining.maxrequests", 8)
    firefox_options.set_preference("network.http.proxy.pipelining", True)
    firefox_options.set_preference("permissions.default.image", 2)
    firefox_options.set_preference("plugin.default_plugin_disabled", False)
    firefox_options.set_preference("reader.parse-on-load.enabled", False)
    return firefox_options
