# python-webscraper

![python-webscraper logo banner](./banner.png)

## Test options

### ENV OPTIONS csv style (don't set if not passing in any options)

1. `headless` (turns on headless mode)
2. any other browser options example: `disable-gpu` (disable gpu)

### Drivers (use `--driver <browser>`)

1. `firefox`
2. `chrome`

Example runs chrome in headless mode:
`OPTIONS=headless pytest --driver chrome tests/*`

## Running from host machine (python3)

### Install required libraries and bins with following commands

1. `pip install -r requirements.txt`
2. `webdrivermanager firefox chrome --linkpath /usr/local/bin`

### Run tests on host machine

1. `pytest --driver chrome tests/*`

## Running from container (container image is saved in dockerhub)

### Start container

1. `docker run --rm -v $(pwd):/runner:z -it casonadams/test-runner`

### Run tests in container

1. `OPTIONS=headless pytest --driver chrome /runner/tests/*`
