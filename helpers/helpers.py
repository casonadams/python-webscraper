import time

from selenium.webdriver.common.action_chains import ActionChains

# TODO: DRY things up a bit
# - wait_for_element_by_link_text
# - wait_for_element_by_css_selector


class helpers:
    """Helper functions for finding and interacting with page elements"""

    def wait_for_element_by_link_text(self, driver, link_text, timeout=5):
        """Waits for element to be found using link text

        Args:
            driver (WebDriver): web driver used for test
            link_text (str): text that represents a link
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for link text
        """

        error = ""
        future_time = time.time() + timeout
        while time.time() < future_time:
            try:
                element = driver.find_element_by_link_text(link_text)
                if element.is_displayed():
                    return element
                raise f"element: {link_text} not visible"
            except Exception as e:
                error = e
                continue
            break
        raise Exception(error)

    def wait_for_element_by_css_selector(self, driver, selector, timeout=5):
        """Waits for element to be found using css selector

        Args:
            driver (WebDriver): web driver used for test
            selector (str): text that represents css selector
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for css selector
        """

        error = ""
        future_time = time.time() + timeout
        while time.time() < future_time:
            try:
                element = driver.find_element_by_css_selector(selector)
                if element.is_displayed():
                    return element
                raise f"element: {selector} not visible"
            except Exception as e:
                error = e
                continue
            break
        raise Exception(error)

    def click_element_by_link_text(self, driver, link_text, timeout=5):
        """Clicks element by link text

        Args:
            driver (WebDriver): web driver used for test
            link_text (str): text that represents link text
            timeout (int): time to wait for element to be availible (default is 5)
        """

        element = self.wait_for_element_by_link_text(driver, link_text, timeout)
        self._scroll_to_selector(driver, element, timeout)
        self._perform_click_action(driver, element)

    def click_element_by_css_selector(self, driver, selector, timeout=5):
        """Clicks element by css selector

        Args:
            driver (WebDriver): web driver used for test
            selector (str): text that represents css selector
            timeout (int): time to wait for element to be availible (default is 5)
        """

        element = self.wait_for_element_by_css_selector(driver, selector, timeout)
        self._scroll_to_selector(driver, element, timeout)
        self._perform_click_action(driver, element)

    # Used example scroll_shim from stackoverflow
    # https://stackoverflow.com/questions/44777053/driver-movetargetoutofboundsexception-with-firefox
    def _scroll_to_selector(self, driver, element, timeout=5):
        if "firefox" in driver.capabilities["browserName"]:
            driver.execute_script(
                f"window.scrollTo({element.location['x']},{element.location['y']});"
            )
            driver.execute_script(
                f"window.scrollBy({element.size['width']}, {element.size['height']});"
            )

    def _perform_click_action(self, driver, element):
        actions = ActionChains(driver)
        actions.move_to_element(element)
        actions.click(element)
        actions.perform()
