from selenium.webdriver.common.action_chains import ActionChains

from helpers.helpers import helpers


class signuppage:
    """Signup page objects and helpers"""

    first_name_selector = "#input-signup-firstName"
    first_name_error_selector = "#error-message-firstName"

    last_name_selector = "#input-signup-lastName"
    last_name_error_selector = "#error-message-lastName"

    mobile_number_selector = "#input-signup-phone"
    business_name_selector = "#input-signup-businessName"
    work_email_selector = "#input-signup-email"
    password_selector = "#input-signup-password"
    create_account_selector = "#create-account-next-btn"

    def url(self):
        """page url

        Returns:
            str: site url

        """

        return "https://signup.podium.com/"

    def first_name(self, driver, timeout=5):
        """first name field element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for first name selector
        """

        return helpers().wait_for_element_by_css_selector(
            driver, self.first_name_selector, timeout
        )

    def first_name_error(self, driver, timeout=5):
        """first name error field element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for first name error selector
        """

        return helpers().wait_for_element_by_css_selector(
            driver, self.first_name_error_selector, timeout
        )

    def insert_first_name(self, driver, first_name, timeout=5):
        """insert string into firstname field

        Args:
            driver (WebDriver): web driver used for test
            first_name (str): text to enter
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().wait_for_element_by_css_selector(
            driver, self.first_name_selector, timeout
        ).send_keys(first_name)

    def last_name_error(self, driver, timeout=5):
        """last name error field element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for last name error selector
        """

        return helpers().wait_for_element_by_css_selector(
            driver, self.last_name_error_selector, timeout
        )

    def insert_last_name(self, driver, last_name, timeout=5):
        """insert string into lastname field

        Args:
            driver (WebDriver): web driver used for test
            last_name (str): text to enter
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().wait_for_element_by_css_selector(
            driver, self.last_name_selector, timeout
        ).send_keys(last_name)

    def click_create_account(self, driver, timeout=5):
        """send click to create account button

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().click_element_by_css_selector(
            driver, self.create_account_selector, timeout
        )
