from helpers.helpers import helpers


class homepage:
    """Home page objects and helpers"""

    def url(self):
        return "https://www.podium.com/"

    def select_login_link(self, driver, timeout=5):
        """selects login link element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().click_element_by_link_text(driver, "Login", timeout)

    def select_get_started_button(self, driver, timeout=5):
        """selects getting started link/button element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().click_element_by_link_text(driver, "Get Started", timeout)
