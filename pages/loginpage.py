from selenium.webdriver.common.action_chains import ActionChains

from helpers.helpers import helpers


class loginpage:
    """Login page objects and helpers"""

    login_field_selector = "#emailOrPhoneInput"
    next_button_selector = "#signInButton"
    required_field_selector = ".sc-jTzLTM"

    def url(self):
        """page url

        Returns:
            str: site url

        """

        return "https://auth.podium.com/"

    def login_field(self, driver, timeout=5):
        """login field element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for login field selector
        """

        return helpers().wait_for_element_by_css_selector(
            driver, self.login_field_selector, timeout
        )

    def missing_email_error(self, driver, timeout=5):
        """login field missing email element

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)

        Returns:
            WebDriver element: element for missing email field selector
        """

        return helpers().wait_for_element_by_css_selector(
            driver, self.required_field_selector, timeout
        )

    def insert_email(self, driver, email, timeout=5):
        """insert string into email/phone field

        Args:
            driver (WebDriver): web driver used for test
            email (str): text to enter
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().wait_for_element_by_css_selector(
            driver, self.login_field_selector, timeout
        ).send_keys(email)

    def click_next(self, driver, timeout=5):
        """send click to next button

        Args:
            driver (WebDriver): web driver used for test
            timeout (int): time to wait for element to be availible (default is 5)
        """

        helpers().click_element_by_css_selector(
            driver, self.next_button_selector, timeout
        )
