from pages.loginpage import loginpage

TIMEOUT = 1


def test_missing_email(driver):
    page = loginpage()
    driver.get(page.url())
    page.click_next(driver)
    page.missing_email_error(driver, TIMEOUT)
