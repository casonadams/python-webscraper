from pages.homepage import homepage
from pages.loginpage import loginpage
from pages.signuppage import signuppage

LONG_TIMEOUT = 10


def test_login_link(driver):
    home = homepage()
    login = loginpage()

    driver.get(home.url())
    home.select_login_link(driver, LONG_TIMEOUT)
    login.login_field(driver)
    assert login.url() == driver.current_url


def test_getting_started_link(driver):
    home = homepage()
    signup = signuppage()

    driver.get(home.url())
    home.select_get_started_button(driver, LONG_TIMEOUT)
    signup.first_name(driver)
    assert signup.url() == driver.current_url
