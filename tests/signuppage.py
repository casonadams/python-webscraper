import pytest

from pages.signuppage import signuppage

# user data for test
USER = {
    "firstname": "Cason",
    "lastname": "Adams",
}

TIMEOUT = 1


def test_missing_firstname(driver):
    page = signuppage()
    driver.get(page.url())
    page.click_create_account(driver)

    # check for require field prompt
    page.first_name_error(driver)
    page.insert_first_name(driver, USER.get("firstname"))
    page.click_create_account(driver)

    # check for require field prompt removal
    with pytest.raises(Exception) as e:
        page.first_name_error(driver, TIMEOUT)
    assert page.first_name_error_selector in str(e.value)


def test_missing_lastname(driver):
    page = signuppage()
    driver.get(page.url())
    page.click_create_account(driver)

    # check for require field prompt
    page.last_name_error(driver)
    page.insert_last_name(driver, USER.get("lastname"))
    page.click_create_account(driver)

    # check for require field prompt removal
    with pytest.raises(Exception) as e:
        page.last_name_error(driver, TIMEOUT)
    assert page.last_name_error_selector in str(e.value)
