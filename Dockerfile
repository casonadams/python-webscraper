FROM registry.fedoraproject.org/fedora-minimal

WORKDIR /tmp

ARG CHROME_REPO=/etc/yum.repos.d/google-chrome.repo
RUN echo '[google-chrome]' > $CHROME_REPO \
  && echo 'name=google-chrome' >> $CHROME_REPO \
  && echo 'baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64' >> $CHROME_REPO \
  && echo 'enabled=1' >> $CHROME_REPO \
  && echo 'gpgcheck=1' >> $CHROME_REPO \
  && echo 'gpgkey=https://dl.google.com/linux/linux_signing_key.pub' >> $CHROME_REPO \
  ;

RUN microdnf install \
  Xvfb \
  google-chrome-stable \
  firefox \
  nss \
  python \
  python-pip \
  && microdnf clean all \
  ;

COPY requirements.txt .

RUN python -m pip install -r requirements.txt \
  ;

RUN webdrivermanager firefox chrome --linkpath /usr/local/bin
